import 'babel-polyfill';
import React from 'react';
import {render} from 'react-dom';
import {browserHistory} from 'react-router';
import {AppContainer} from 'react-hot-loader';

import configureStore from './app/redux/create';
import App from './app/components/App/App';

const store = configureStore();

render(
  <AppContainer>
    <App store={store} history={browserHistory}/>
  </AppContainer>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept('./app/components/App/App', () => {
    const RootContainer = require('./app/components/App/App').default;
    render(
      <AppContainer>
        <RootContainer store={store} history={browserHistory}/>
      </AppContainer>,
      document.getElementById('root')
    );
  });
}

if (module.hot) {
  const orgError = console.error; // eslint-disable-line no-console
  console.error = message => { // eslint-disable-line no-console
    if (message && message.indexOf('You cannot change <Router') === -1) {
      orgError.apply(console, [message]);
    }
  };
}
