export let BASE_URL = '';

if (process.env.NODE_ENV !== 'production') {
  BASE_URL = process.env.APIHOST || 'http://localhost:3000';
}

export const TOKEN_NAME = 'mdr_token_id';
export const API_URL = `${BASE_URL}/api`;
