import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import api from './middleware/api';
import rootReducer from './modules/reducer';

export default () => {
  const createStoreWithMiddleware = applyMiddleware(thunkMiddleware, api)(createStore);
  const store = createStoreWithMiddleware(rootReducer, window.devToolsExtension && window.devToolsExtension());
  if (module.hot) {
    module.hot.accept('./modules/reducer', () => {
      const nextReducer = require('./modules/reducer').default;
      store.replaceReducer(nextReducer);
    });
  }

  return store;
};
