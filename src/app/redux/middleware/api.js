import 'whatwg-fetch';
import {TOKEN_NAME, API_URL as BASE_URL} from '../../../config';
import {browserHistory} from 'react-router';

function callApi(endpoint, authenticated = true, body, method) {
  const token = localStorage.getItem(TOKEN_NAME) || null;
  let config = {};

  if (authenticated) {
    if (token) {
      config = {
        headers: {Authorization: `Bearer ${token}`, 'Content-Type': 'application/json'}
      };
    } else {
      throw new Error('No token saved!');
    }
  } else {
    config = {
      headers: {'Content-Type': 'application/json'}
    };
  }

  if (body) {
    config.method = method || 'POST';
    config.body = JSON.stringify(body);
  }

  return fetch(BASE_URL + endpoint, config)
    .then(response => {
      if (!response.ok) {
        if (response.status === 401) {
          browserHistory.push('/logout');
        }
        return response.text().then(txt => {
          const err = `Error: (${response.status}) ${response.statusText} ${txt}`;
          return Promise.reject(err);
        });
      }
      return response.json().then(payload => payload);
    });
}

export const CALL_API = Symbol('Call API');

export default ({dispatch}) => next => action => {
  const callAPI = action[CALL_API];

  if (typeof callAPI === 'undefined') {
    return next(action);
  }

  const {endpoint, types, authenticated, body, method} = callAPI;

  const [requestType, successType, errorType] = types;

  dispatch(Object.assign({}, {type: requestType}));

  return callApi(endpoint, authenticated, body, method)
    .then(response =>
      next({
        response,
        authenticated,
        type: successType
      }),
      error => {
        const err = error || 'There was an error.';
        console.log('---error---', err);
        return next({
          error: err,
          type: errorType
        });
      }
    );
};
