const module = 'sample';

import {CALL_API} from '../middleware/api';

const REQUEST = `${module}/REQUEST`;
const REQUEST_SUCCESS = `${module}/REQUEST_SUCCESS`;
const REQUEST_FAIL = `${module}/REQUEST_FAIL`;

const initialState = {
  isFetching: false,
  data: [],
  error: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: null
      });
    case REQUEST_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        data: action.response.data,
        error: action.response.error
      });
    case REQUEST_FAIL:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.response
      });
    default:
      return state;
  }
}

export function getData(body) {
  return {
    [CALL_API]: {
      endpoint: `/${module}/list`,
      body,
      authenticated: false,
      types: [REQUEST, REQUEST_SUCCESS, REQUEST_FAIL]
    }
  };
}
