import React, {PropTypes} from 'react';
import {Provider} from 'react-redux';
import {Router} from 'react-router';
import getRoutes from '../../../routes';

const App = ({store, history}) => (
  <Provider store={store}>
    <Router history={history}>
      {
        getRoutes
      }
    </Router>
  </Provider>
);

App.propTypes = {
  store: PropTypes.object,
  history: PropTypes.object
};

export default App;
