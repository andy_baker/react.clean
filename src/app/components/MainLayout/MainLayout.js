import React, {PropTypes} from 'react';

const MainLayout = ({children}) => (
  <div className="app container">
    <main>
      {
        children
      }
    </main>
  </div>
);

MainLayout.propTypes = {
  children: PropTypes.object
};

export default MainLayout;
