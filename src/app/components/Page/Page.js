import React, {PropTypes} from 'react';
import Lorem from 'react-lorem-component';
import styles from './Page.css';

const Page = ({route: {header}}) => (
  <div>
    <h4>{header}</h4>
    <div className={styles.text}>
      <Lorem seed={Math.random() * 100}/>
    </div>
  </div>
);

Page.propTypes = {
  route: PropTypes.object
};

export default Page;
