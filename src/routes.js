import React from 'react';
import {IndexRoute, Route} from 'react-router';

import MainLayout from './app/components/MainLayout/MainLayout';
import NotFound from './app/components/NotFound/NotFound';
import Page from './app/components/Page/Page';

export default (
  <Route path="/" component={MainLayout}>
    <IndexRoute component={Page} header="Home"/>
    <Route path="*" component={NotFound} status={404}/>
  </Route>
);
